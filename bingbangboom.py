import os
import redis
import flask
import json
import urlparse
from flask import Flask, Response, request, render_template, abort
from flask.ext.cors import CORS, cross_origin

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'
redis_handle = redis.Redis('localhost')

@app.route('/')
@cross_origin()
def hello():
    return 'BingBangBoom'


@app.route('/device/<device_id>', methods=['GET'])
@cross_origin()
def get_role(device_id):
    response = {}
    speaker = redis_handle.get(device_id)
    if not speaker:
        response["msg"] = "no user found"
        return Response(json.dumps(response), status=404, mimetype="application/json")
    if str(speaker) == "A":
        return "Bing"
    elif str(speaker) == "B":
        return "Bang"
    elif str(speaker) == "A&B":
        return "Boom"
    else:
        return "Meh"


@app.route('/device', methods=['POST'])
@cross_origin()
def map_device():

    data = request.get_json(force=True)
    id = data["device_id"]
    response = {}
    map  = ''
    if id:
        if id % 3 == 0:
            map = 'A'
        elif id % 5 == 0:
            map = 'B'
        elif id % 3 == 0 & id % 5 == 0:
            map = 'A&B'

        redis_handle.set(id, map)
        return Response(status=201)
    else:
        response["msg"] = "required device id not found"
        return Response(json.dumps(response), status=400)



@app.route('/clear', methods=['GET'])
@cross_origin()
def clear_data():
    redis_handle.flushall()
    return "ok!"


if __name__ == "__main__":
    app.run(debug=True)
