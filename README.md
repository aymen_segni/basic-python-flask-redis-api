### The BingBangBoom Game:

Let’s define a game called BingBangBoom for which we set up two speakers, A and B.

Each speaker requests its audio mapping through an API in the server, providing its unique device ID​. The server can map a speaker to audio domain ​A​ and/or ​B​.
* Speakers with device IDs that are multiples of 3 are mapped to domain ​A
* Speakers with device IDs that are multiples of 5 are mapped to domain ​B
* All other speakers are not mapped to any domain

The speakers assign themselves the roles “​Bing​”, “​Bang​” or “​Boom​”, according to
that audio mapping. The possible scenarios are:
* Speakers that are mapped to domain ​A​ shall get the role “​Bing​”
* Speakers that are mapped to domain ​B​ shall get the role “​Bang​”
* Speakers that are mapped to domain ​A&B​ shall get the role “​Boom​”
* Speakers that are not mapped to any domain shall get the role “​Meh​”

### Implementation: 

Alright, let the games begin!

To develop the game We'll build a Restfull API using Flask, a microframework for Python and  using Redis as Database Management System.

I've named the app BingBangBoom, I know, creative, right! 

The PAI will have the following features:

1.  Map the device role: providing its unique device ID​ , the server can map a speaker to audio domain ​A​ and/or ​B​ thru a POST method and we will store the map value in Redis.
2.  Get the device role: according to that audio mapping. The speakers will be assign the roles “​Bing​”, “​Bang​”, “​Boom​” or. Meh. Thru a GET method we get the map value from redis by the unique device id. 


### Setup Instructions
1. `cd basic-python-flask-redis-api` 
2. Start virtualenv -- `.venv/bin/activate`
3. `pip install -r requirements.txt` -- this will install dependencies already specified in `requirements.txt` file
4. `python bingbangboom.py` -- this will start the server.
5. start redis server -- `reids-server`

### Example Queries
#### Upsert and map device to server into redis
POST `http://127.0.0.1:5000/device`
```json
{
 "device_id" : 333
}
```
* Op successful - StatusCode: 201 CREATED


#### Get the device role [bing, bang, boom, Meh] from Redis by the device_id:
GET `http://127.0.0.1:5000/device?/<device_id>`

Example : GET `http://127.0.0.1:5000/device?/333`

Response object : **Bing**

* Get successful - StatusCode: 200 OK
* Nothing found - StatusCode: 404 NOTFOUND


### (Optional) Dockerized the app: Docker build & Deployment 

1. Build it: -- `docker build -t .`
2. Run it: --  `docker-compose up`
3. 

### (Optional) Deployment with Supervisor

1. Install supervisord:

`sudo apt install supervisor` (for debian and ubuntu)

`sudo yum install -y supervisor` (for rehel family )


Take a look at the configuration file /etc/supervisor/supervisor.conf, at the end there are a couple of lines that should not be commented:

```
[include]
files = /etc/supervisor/conf.d/*.conf
```

It is a very good practice to make a separate configuration file for each of our programs, these files go in /etc/supervisor/conf.d/, let’s make the config file for BingBangBoom Game

```
[program:bingbangboom]
command=path/to/virtual/environment/bin/python path/to/binbang/bingbangboom.py
autostart=true
autorestart=true
killasgroup=false
stderr_logfile=/var/log/supervisor/bingbangboom.log

```
